from typing import List
import numpy as np
from src.point import Point, route_length


def bruteforce(points: List[Point], starting_point: int = 0, current_route=None, verbose: bool = False, procedure=print):
    if current_route is None:
        current_route = [points[starting_point]]
    if len(current_route) == len(points):
        current_route.append(points[starting_point])
        cr_length = route_length(current_route)
        if verbose:
            procedure(current_route, cr_length)
        return current_route, cr_length
    min_route = List[Point]
    min_distance = -1
    current_sub_route = current_route.copy()
    for point in points:
        if point not in current_route:
            current_route = current_sub_route.copy()
            current_route.append(point)
            result = bruteforce(points, starting_point, current_route, verbose, procedure)
            if min_distance == -1 or min_distance > result[1]:
                min_route = result[0]
                min_distance = result[1]
    return min_route, min_distance


def genetic_algorithm(points: List[Point], max_generation: int, mutation_chance: float, sample_number: int = 5):
    pass
